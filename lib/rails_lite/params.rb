require 'uri'

class Params
  attr_accessor :params

  def initialize(req, route_params)
    @req = req
    body_params = parse_www_encoded_form(@req.body)
    @params = parse_www_encoded_form(@req.query_string)
    @params = @params.merge(body_params)
  end

  def [](key)
    @params[key]
  end

  def to_s
  end

  private
  def parse_www_encoded_form(www_encoded_form)
    if www_encoded_form != nil
      params_arr = URI::decode_www_form(www_encoded_form)
      hash = {}
      params_arr.each { |arr| hash[arr[0]] = arr[1] }
    end

    return {} unless hash
    hash
  end

  def parse_key(key)
    key.split(/\]\[|\[|\]/)
  end
end
