require 'erb'
require_relative 'params'
require_relative 'session'
require 'active_support/core_ext'

class ControllerBase
  attr_reader :params


  def initialize(req, res, route_params = {})
    @req = req
    @res = res
    @params = Params.new(@req, route_params).params
  end

  def render_content(body, content_type)
    @res.body = body
    @res.content_type = content_type
    @already_built_response = true
  end

  def redirect_to(url)
    @res.status = '302'
    @res.header['location'] = url.to_s
    session.store_session(@res)
    @already_built_response = true
  end

  def render(template_name)
    controller_name = "#{self.class}".underscore
    view_file = File.read("views/#{controller_name}/#{template_name}.html.erb")
    template = ERB.new(view_file).result(binding)
    session.store_session(@res)
    render_content(template, 'text/text')
  end

  def session
    @session ||= Session.new(@req)
  end

  def already_rendered?
  end

  def invoke_action(name)
  end
end
